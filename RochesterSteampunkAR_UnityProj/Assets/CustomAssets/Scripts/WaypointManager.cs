﻿using System.Collections;
using System.Collections.Generic;
using Mapbox;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Geocoding;
using UnityEngine;





public class WaypointManager : MonoBehaviour
{
    /// <summary>
    /// Spawn waypoints based on game data from GameDataManager
    /// Positions converted to worldspace locations based on map coordinates
    /// These waypoints aren't actually a part of the map/mapbox data
    /// </summary>

    [SerializeField]
    private GameObject waypointPrefab;
    [SerializeField]
    private GameDataManager gdM;
    [SerializeField]
    private AbstractMap curMap;
    [SerializeField]
    private bool mapInitialized, waypointsSpawned;
    [SerializeField]
    private List<GameObject> activeWaypoints;
    [SerializeField] private GameDataParser dataParser;

    // Start is called before the first frame update

    void Start()
    {
        activeWaypoints = new List<GameObject>();
    }

    private void OnEnable()
    {
        curMap.OnInitialized += SpawnWaypoints;
    }

    private void OnDisable()
    {
        curMap.OnInitialized -= SpawnWaypoints;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnWaypoints()
    {
        // Iterate through dictionary, spawn waypoint for each key/value location/coordinate pair
        foreach (KeyValuePair<string, Vector2> locationEntry in gdM.locationDictionary)
        {
            // Convert map coordinates to unity world space value for positioning waypoint
            Vector2d prefabPos;
            prefabPos = Conversions.GeoToWorldPosition(locationEntry.Value.x, locationEntry.Value.y, curMap.CenterMercator, curMap.WorldRelativeScale);

            // Spawn the waypoint, assign to a variable 
            GameObject tempWaypoint = (GameObject)Instantiate(waypointPrefab, new Vector3((float)prefabPos.x, 0, (float)prefabPos.y), Quaternion.identity);

            // Access current waypoint waypointdata component
            WaypointData tempWaypointData = tempWaypoint.GetComponent<WaypointData>();

            // Format the current location name string to match parsed JSON format (ie. Police Station > policestation)
            string formattedWaypointLocationTitle = locationEntry.Key.ToLower();
            formattedWaypointLocationTitle = formattedWaypointLocationTitle.Replace(" ", "");
            // Debug.Log(formattedWaypointLocationTitle);

            tempWaypointData.locationTitle = locationEntry.Key;
            tempWaypointData.storySegmentsAtLocation = new List<StorySection>();

            // Loop through all story segments, assign waypoint all segments attached to location
            for (int i = 0; i < dataParser.storyList.StorySection.Length; i++)
            {
                if(dataParser.storyList.StorySection[i].location == formattedWaypointLocationTitle)
                {
                    tempWaypointData.storySegmentsAtLocation.Add(dataParser.storyList.StorySection[i]);
                }
            }

            // Finally, add the waypoint to a list of active waypoints
            activeWaypoints.Add(tempWaypoint);
        }

    }
}
