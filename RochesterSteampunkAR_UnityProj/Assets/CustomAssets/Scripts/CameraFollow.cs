﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    /// <summary>
    /// Make the camera follow the player.
    /// Motion/Rotation smoothed via interpolation
    /// </summary>

    public CameraManager camManager;
    public GameObject followTarget, closeViewTarget, overheadViewTarget;

    [SerializeField]
    private bool followPlayer;
    [SerializeField]
    private Transform lookAtTransform;
    [SerializeField]
    private string curViewType;
    [SerializeField]
    private Quaternion overheadViewRotation, curCamRotation;
    [SerializeField]
    private float smoothTime = 0.5f;
    [SerializeField]
    private Vector3 velocity = Vector3.zero;
    [SerializeField]
    private float lerpVal, lerpRate;

    void Start()
    {
        lerpRate = 2f;
        followPlayer = true;
        followTarget = closeViewTarget;
        curViewType = "closeView";
        overheadViewRotation = overheadViewTarget.transform.rotation;

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Called after Update each frame
    private void LateUpdate()
    {
        if (followPlayer)
        {
            transform.position = Vector3.SmoothDamp(transform.position, followTarget.transform.position, ref velocity, smoothTime);

            if(curViewType == "overheadView")
            {
                if (Vector3.Distance(transform.eulerAngles, overheadViewRotation.eulerAngles) > .001f)
                {
                    lerpVal += Time.deltaTime;
                    transform.rotation = Quaternion.Lerp(curCamRotation, overheadViewRotation, Mathf.SmoothStep(0, 1, lerpVal * 5f));
                }
            } else
            {
                transform.localRotation = Quaternion.Lerp(transform.localRotation, (Quaternion.LookRotation(lookAtTransform.position - transform.position)), Time.deltaTime * 10f);
                //transform.LookAt(lookAtTransform);
            }
        }

    }

    //Change view type to opposite of current view (close/far)
    
    public void ChangeViewType ()
    {
        switch (curViewType)
        {
            case "closeView":
                followTarget = overheadViewTarget;
                curViewType = "overheadView";
                curCamRotation = gameObject.transform.rotation;
                lerpVal = 0;
                break;
            case "overheadView":
                followTarget = closeViewTarget;
                curViewType = "closeView";
                break;
        }
    }



    
}

