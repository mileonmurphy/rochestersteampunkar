﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UImanager : MonoBehaviour
{
    [SerializeField] private CanvasGroup walkingUI, narrativeUI;
    [SerializeField] private TextMeshProUGUI locationTitleTMP, narrativeTextTMP;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleNarrativeUI()
    {
        switch (narrativeUI.gameObject.activeSelf)
        {
            case true:
                narrativeUI.gameObject.SetActive(false);
                break;
            case false:
                narrativeUI.gameObject.SetActive(true);
                break;
        }
    }

    public void PopulateNarrativeUI (WaypointData curWaypoint)
    {
        locationTitleTMP.text = curWaypoint.locationTitle; 
        narrativeTextTMP.text = curWaypoint.storySegmentsAtLocation[0].text;
    }


}
