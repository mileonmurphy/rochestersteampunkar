﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private UImanager uIM;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CollectWaypointStorySegment(WaypointData sendData)
    {
        uIM.PopulateNarrativeUI(sendData);
        uIM.ToggleNarrativeUI();
    }


}
