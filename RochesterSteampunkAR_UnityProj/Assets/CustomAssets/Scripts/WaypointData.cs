﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointData : MonoBehaviour
{
    public string locationTitle;
    public List<StorySection> storySegmentsAtLocation;


    [SerializeField] private WaypointAnimator animator;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if(animator.curWaypointState == WaypointAnimator.WaypointState.activated)
        {
            animator.player.CollectWaypointStorySegment(this);
        }
    }
}
