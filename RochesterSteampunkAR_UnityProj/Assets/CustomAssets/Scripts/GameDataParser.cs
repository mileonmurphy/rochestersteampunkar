﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class GameDataParser : MonoBehaviour
{
    /// <summary>
    /// Parse game info from coordinate and narrative text files
    /// Data stored in GameDataManager
    /// </summary>

    [SerializeField]
    private GameDataManager gdManager;
    [SerializeField]
    private StreamReader streamReader;
    [SerializeField]
    private Stream textFileStream;
    [SerializeField]
    private string coordinateFilePath, narrativeFilePath, inputLine;
    [SerializeField]
    private List<string> gameDataFiles, fileInput;
    [SerializeField]
    private WaypointManager wM;
    [SerializeField]
    private string storyJson;
    public StorySectionList storyList;
    // Start is called before the first frame update
    void Start()
    {
        fileInput = new List<string>();
        GetFilePaths();
        gdManager.locationDictionary = new Dictionary<string, Vector2>();
        //gdManager.narrativeDictionary = new Dictionary<string, string>();
        ReadFiles(coordinateFilePath, "locations");
        storyJson = File.ReadAllText(narrativeFilePath);
        ReadStoryFile();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GetFilePaths()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            string realPath = Application.persistentDataPath + "LocationCoordinates.txt";
            if (!System.IO.File.Exists(realPath))
            {
                if (!System.IO.Directory.Exists(Application.persistentDataPath + "/PATH/"))
                {
                    System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/PATH/");
                }
                WWW reader = new WWW(Application.streamingAssetsPath + "/LocationCoordinates.txt");
                while (!reader.isDone) { }
                System.IO.File.WriteAllBytes(realPath, reader.bytes);
            }
            //Application.OpenURL(realPath);
            coordinateFilePath = realPath;

            realPath = Application.persistentDataPath + "StoryText.json";
            if (!System.IO.File.Exists(realPath))
            {
                if (!System.IO.Directory.Exists(Application.persistentDataPath + "/PATH/"))
                {
                    System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/PATH/");
                }
                WWW reader = new WWW(Application.streamingAssetsPath + "/StoryText.json");
                while (!reader.isDone) { }
                System.IO.File.WriteAllBytes(realPath, reader.bytes);
            }
            //Application.OpenURL(realPath);
            narrativeFilePath = realPath;

        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {

        } else
        {
            coordinateFilePath = Application.streamingAssetsPath + "/LocationCoordinates.txt";
            narrativeFilePath = Application.streamingAssetsPath + "/StoryText.json";
        }


    }

    //not using streamreader for story file, remove dual use for this method
    private void ReadFiles(string filePath, string targetDictionary)
    {
        fileInput.Clear();
        
        switch(targetDictionary)
        {
            case "locations":
                streamReader = new StreamReader(coordinateFilePath);
                while((inputLine = streamReader.ReadLine()) != null)
                {
                    fileInput.Add(inputLine);
                }

                for (int i = 1; i < fileInput.Count -1; i++)
                {
                    if (fileInput[i].Length > 1)
                    {
                        string[] inputLineArray = fileInput[i].Split(new char[] { ',' });


                        float tempX, tempY;
                        string formattedLocationName = inputLineArray[0].Replace("_", " ");

                        float.TryParse(inputLineArray[1], out tempX);
                        float.TryParse(inputLineArray[2], out tempY);
                        gdManager.locationDictionary.Add(formattedLocationName, new Vector2(tempX, tempY));
                       // Debug.Log("Added new dictionary entry");
                    }
                }
                break;
        }
    }

    private void ReadStoryFile ()
    {

        storyList = JsonUtility.FromJson<StorySectionList>(storyJson);

        //Debug.Log("JSON parse result: " + storyList.StorySection.Length);
        //for (int i = 0; i < storyList.StorySection.Length; i++)
        //{
        //    Debug.Log("Scenario: " + storyList.StorySection[i].scenario + " Faction: " + storyList.StorySection[i].faction + " Location: " + storyList.StorySection[i].location + " Text: " + storyList.StorySection[i].text);
        //}
    }

    //for testing
    private void DisplayDictionaryEntries()
    {
        foreach (KeyValuePair<string, Vector2> entry in gdManager.locationDictionary)
        {
            Debug.Log("Location: " + entry.Key + " Coordinates: " + entry.Value.x + ", " + entry.Value.y);
        }
    }
}

[System.Serializable]
public class StorySection
{
    public string scenario, faction, location, text;
}

[System.Serializable]
public class StorySectionList
{
    public StorySection[] StorySection;
}
