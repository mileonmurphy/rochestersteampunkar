﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dest.Modeling;

public class WaypointAnimator : MonoBehaviour
{
    /// <summary>
    /// Animate the waypoint model
    /// Resting, Activated, Deactivated
    /// </summary>

   public enum WaypointState { resting, activating, deactivating, activated  }

    [SerializeField]
    public WaypointState curWaypointState;
    [SerializeField]
    private GameObject sphere, cube, torus, sphereEndOBJ, cubeEndOBJ, torusEndOBJ;
    [SerializeField]
    private Vector3 sphereStart, sphereEnd, cubeEnd,
        torusStart, torusEnd;
    [SerializeField]
    private float lerpVal, sphereLerpVal;
    [SerializeField]
    private float lerpRate = .5f;
    [SerializeField]
    private bool sphereUp;
    [SerializeField]
    private Quaternion cubeStart;
    [SerializeField] private Torus curTorusGenerator;

    public PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        //curTorusGenerator = gameObject.GetComponent<Torus>();
       // curTorusGenerator.CreateMesh();
        sphereStart = sphere.transform.position;
        //cubeStart = cube.transform.rotation;
        torusStart = torus.transform.localScale;
        torusEnd = torusEndOBJ.transform.localScale;
        cubeEnd = cubeEndOBJ.transform.rotation.eulerAngles;
        sphereEnd = sphereEndOBJ.transform.position;
        sphereUp = true;

        curWaypointState = WaypointState.resting;
       // Invoke("ActivateWaypoint", 5f);
        //Invoke("DeactivateWaypoint", 10f);
    }

    // Update is called once per frame
    void Update()
    {
        switch (curWaypointState)
        {
            case WaypointState.resting:
                sphereLerpVal += Time.deltaTime;
                switch (sphereUp) {
                    case true:
                    sphere.transform.position = Vector3.Lerp(sphereStart, sphereEnd, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereEnd) <= .001f)
                        {
                            sphereUp = false;
                            sphereLerpVal = 0;
                        }
                        break;
                    case false:
                        sphere.transform.position = Vector3.Lerp(sphereEnd, sphereStart, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereStart) <= .001f)
                        {
                            sphereUp = true;
                            sphereLerpVal = 0;
                        }
                        break;
        }
                
                cube.transform.eulerAngles += new Vector3(0, (15 * Time.deltaTime) , 0);
                break;

            case WaypointState.activating:
                lerpVal += Time.deltaTime;
                torus.transform.localScale = Vector3.Lerp(torusStart, torusEnd, Mathf.SmoothStep(0, 1, lerpVal / lerpRate));
                cube.transform.rotation = Quaternion.Lerp(cubeStart, cubeEndOBJ.transform.rotation, Mathf.SmoothStep(0, 1, lerpVal / lerpRate));

                if((Vector3.Distance(torus.transform.localScale, torusEnd) <= .001f) && (Vector3.Distance(cube.transform.eulerAngles, cubeEnd) <= .001f))
                {
                    curWaypointState = WaypointState.activated;
                    lerpVal = 0;
                }

                sphereLerpVal += Time.deltaTime;
                switch (sphereUp)
                {
                    case true:
                        sphere.transform.position = Vector3.Lerp(sphereStart, sphereEnd, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereEnd) <= .001f)
                        {
                            sphereUp = false;
                            sphereLerpVal = 0;
                        }
                        break;
                    case false:
                        sphere.transform.position = Vector3.Lerp(sphereEnd, sphereStart, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereStart) <= .001f)
                        {
                            sphereUp = true;
                            sphereLerpVal = 0;
                        }
                        break;
                }

                // stop moving, torus expands, square rotates
                // stop animating when finished
                break;
            case WaypointState.deactivating:
                lerpVal += Time.deltaTime;
                torus.transform.localScale = Vector3.Lerp(torusEnd, torusStart, Mathf.SmoothStep(0, 1, lerpVal / lerpRate));
                cube.transform.rotation = Quaternion.Lerp(cubeEndOBJ.transform.rotation, cubeStart, Mathf.SmoothStep(0, 1, lerpVal / lerpRate));

                if ((Vector3.Distance(torus.transform.localScale, torusStart) <= .001f) && (Vector3.Distance(cube.transform.eulerAngles, cubeStart.eulerAngles) <= .001f))
                {
                    curWaypointState = WaypointState.resting;
                    lerpVal = 0;
                }

                sphereLerpVal += Time.deltaTime;
                switch (sphereUp)
                {
                    case true:
                        sphere.transform.position = Vector3.Lerp(sphereStart, sphereEnd, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereEnd) <= .001f)
                        {
                            sphereUp = false;
                            sphereLerpVal = 0;
                        }
                        break;
                    case false:
                        sphere.transform.position = Vector3.Lerp(sphereEnd, sphereStart, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereStart) <= .001f)
                        {
                            sphereUp = true;
                            sphereLerpVal = 0;
                        }
                        break;
                }
                //reverse torus/cube animation
                // switch to resting state
                break;
            case WaypointState.activated:
                sphereLerpVal += Time.deltaTime;
                switch (sphereUp)
                {
                    case true:
                        sphere.transform.position = Vector3.Lerp(sphereStart, sphereEnd, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereEnd) <= .001f)
                        {
                            sphereUp = false;
                            sphereLerpVal = 0;
                        }
                        break;
                    case false:
                        sphere.transform.position = Vector3.Lerp(sphereEnd, sphereStart, Mathf.SmoothStep(0, 1, sphereLerpVal / lerpRate));
                        if (Vector3.Distance(sphere.transform.position, sphereStart) <= .001f)
                        {
                            sphereUp = true;
                            sphereLerpVal = 0;
                        }
                        break;
                }
                torus.transform.eulerAngles += new Vector3(0, (15 * Time.deltaTime), 0);


                break;
        }
    }

    public void ActivateWaypoint()
    {
        curWaypointState = WaypointState.activating;
        cubeStart = cube.transform.rotation;
    }

    public void DeactivateWaypoint()
    {
        curWaypointState = WaypointState.deactivating;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (curWaypointState != WaypointState.activating && curWaypointState != WaypointState.activated)
            {
                player = other.gameObject.GetComponent<PlayerController>();
                ActivateWaypoint();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            DeactivateWaypoint();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if (curWaypointState != WaypointState.activating && curWaypointState != WaypointState.activated)
            {
                player = other.gameObject.GetComponent<PlayerController>();
                ActivateWaypoint();
            }
        }
    }
}
